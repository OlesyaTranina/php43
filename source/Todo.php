<?php
namespace source; 

class Todo{
	private $pdo = null;
	function __construct($pdo){
		$this->pdo = $pdo->getPdo();
	}
	public function getFullTable($orderField=""){
		
		$tsql = "SELECT task.`id`,task.`description`,task.`is_done` ,task.`date_added`, userautor.`login` as autor, userDestination.`login` as destination FROM task
		LEFT JOIN user as userautor on task.`user_id` = userautor.`id`
		LEFT JOIN user as userDestination on task.`assigned_user_id` = userDestination.`id`
		where task.`user_id` = :user
		";
		if (!$orderField=="") {
			$tsql = $tsql . " ORDER BY ". $orderField;
		}
		$res = $this->pdo->prepare($tsql) ;
		$res->bindValue(':user', $_SESSION["user_id"] , \PDO::PARAM_INT);		  
		return $res;
	}
	public function addRecord($description,$is_done){
  
		$res = $this->pdo->prepare("INSERT  INTO  task (`description`,`is_done`,`date_added`,`user_id`,`assigned_user_id`) VALUES (:description, :is_done, now(), :user, :user)") ;
		$res->bindValue(':description', htmlspecialchars($description) , \PDO::PARAM_STR);
		$res->bindValue(':is_done', htmlspecialchars($is_done) , \PDO::PARAM_INT);
		$res->bindValue(':user', $_SESSION["user_id"] , \PDO::PARAM_INT);		  
		$res->execute();
	}
	public function changeRecord($id,$description,$is_done,$assignUser){
   	$res = $this->pdo->prepare("UPDATE task SET `description` = :description,
			`is_done`     = :is_done,
			`assigned_user_id`  = :assigned_user_id
			where id = :id      ") ;
   	$res->bindValue(':description', htmlspecialchars($description) , \PDO::PARAM_STR);
		$res->bindValue(':is_done', $is_done , \PDO::PARAM_INT);
		$res->bindValue(':assigned_user_id', $assignUser , \PDO::PARAM_INT);
		$res->bindValue(':id', $id , \PDO::PARAM_INT);
		$res->execute();
	}
	public function delRecord($id){

		$res = $this->pdo->prepare("DELETE  FROM task 
																where id = :id      ") ;
		$res->bindValue(':id', $id , \PDO::PARAM_INT);
		$res->execute();
	}
	public function getForeignTasks($orderField=""){
		$tsql = "SELECT task.`id`,task.`description`,task.`is_done` ,task.`date_added`, userautor.`login` as autor, userDestination.`login` as destination FROM task
		LEFT JOIN user as userautor on task.`user_id` = userautor.`id`
		LEFT JOIN user as userDestination on task.`assigned_user_id` = userDestination.`id`
		where task.`assigned_user_id` = :user and task.`user_id` <> :user
		";
		if (!$orderField=="") {
			$tsql = $tsql . " ORDER BY ". $orderField;
		}
		$res = $this->pdo->prepare($tsql) ;
		$res->bindValue(':user', $_SESSION["user_id"] , \PDO::PARAM_INT);		  
		return $res;
	}
	
}





