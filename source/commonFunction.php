<?php
namespace source; 

function getHeader() {
	return array("id"=>"номер",
		"description" =>"описание",
		"is_done"=>"Выполнено",
		"date_added"=>"добавлено",
		"autor"=>"Автор",
		"destination"=>"ответственный");
}

function getEditField() {
	return array("description" =>"Содержание задачи","is_done"=>"Выполнено","destination"=>"ответственный");
}

function getShowField() {
	return array("id"=>"номер","date_added"=>"дата добавления","autor"=>"Автор");
}
