<?php
namespace source; 

class Users{
	private $pdo = null;
	function __construct($pdo){
		$this->pdo = $pdo->getPdo();
	}
	public function findUser($login){
		$res = $this->pdo->prepare("SELECT `id`,`login`,`password`   FROM user WHERE `login` = :login");
		$res->bindValue(':login', htmlspecialchars($login) , \PDO::PARAM_STR);
		$res->execute();
		return $res->fetch(\PDO::FETCH_ASSOC);
	}

	public function addUser($login,$password){
		$res = $this->pdo->prepare("INSERT  INTO user (`login`,`password`) VALUES (:login,:password)  ") ;
		$res->bindValue(':login', htmlspecialchars($login) , \PDO::PARAM_STR);
		$res->bindValue(':password', htmlspecialchars($password) , \PDO::PARAM_STR);
		$res->execute();
	}

	public function getStrSelect($login) {
		$res = $this->pdo->prepare("SELECT `id`,`login` FROM user ORDER BY `login` ") ;
		$res->execute();	
		$arr = $res->fetchAll();
		$strSelect = "";
		foreach (	$arr  as $key => $value) {	
			if ($value["login"] == $login) {
				$strSelect =$strSelect."<option selected value=".$value["id"].">".$value["login"]."</option>";
			}	else {
				$strSelect =$strSelect."<option value=".$value["id"].">".$value["login"]."</option>";
			}
		}  
		return "<select name=\"assigned_user_id\">". $strSelect. "</select>";
	}

}	