<?php
namespace source; 

class SqlConnect{
	private $pdo = null;
	function __construct($dbName,$login,$password){
		$this->pdo = new \PDO("mysql:host=localhost;dbname=" . $dbName, $login, $password);
		$this->pdo->query("SET NAMES UTF8");
	}
	public function getPdo(){
		return $this->pdo;
	}
}	