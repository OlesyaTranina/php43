<?php
session_start();
include "source/commonFunction.php";

spl_autoload_register(function () {
	$filePath = str_replace('\\', DIRECTORY_SEPARATOR, __DIR__ . '/source/Textsql.php');
	if (file_exists($filePath)) {
			include $filePath;
	}
});

spl_autoload_register(function ($className) {
	$filePath = str_replace('\\', DIRECTORY_SEPARATOR, __DIR__ . '/source/' .		$className . '.php');
	if (file_exists($filePath)) {
				include $filePath;
	}
});
spl_autoload_register(function ($fullClassName) {
	$filePath = str_replace('\\', DIRECTORY_SEPARATOR, __DIR__ . '/' .	$fullClassName . '.php');
	if (file_exists($filePath)) {
		include $filePath;
	}
});

use source\Todo;
use source\SqlConnect;
use source\Users;


if (!$_SESSION["name"]) {
	header("Location: login.php",true);
}

$db = new SqlConnect("tranina","tranina","neto0391");
//$db = new SqlConnect("hw43","trole","trole");

$tasks = new Todo($db);

if (isset($_POST["submitAdd"])) {
	$tasks->addRecord($_POST["description"],(isset($_POST["is_done"]))?1:0);
} else {if (isset($_POST["submitChange"])){
	$tasks->changeRecord($_POST["id"],$_POST["description"],(isset($_POST["is_done"]))?1:0, $_POST["assigned_user_id"]);
} else {if (isset($_POST["submitDel"])){
	$tasks->delRecord($_POST["id"]);
};
};
} 
if (isset($_POST["submitAdd"])||isset($_POST["submitChange"])||isset($_POST["submitDel"])) {
	header("Location: index.php",true);
}

$arrHeader = source\getHeader();
$strOrder = "";
foreach ($arrHeader as $key => $value) {
	if (isset($_POST["or_".$key])) {
		$strOrder = $key;
	};
}
$users = new Users($db);

$sqlRes = $tasks->getFullTable($strOrder);
$sqlRes->execute();
$editField = source\getEditField();
$ForeignTasks = $tasks->getForeignTasks($strOrder);
$ForeignTasks->execute();
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>4.3 Задание к занятию «Запросы из нескольких»</title>
	<style type="text/css">
		form {
			margin: 10px 0;	
		}
		.row {
			margin: 0 0;	
		}
		
		div {
			display: table;
		}
		ul {
			display: table-row;
		}
		li {
			display: table-cell;
			padding: 0 5px;
			vertical-align: middle;
			border: 1px solid;
			width:200px;
		}
		.header {
			font-weight: 700;
			text-align: center;
			background-color: lightblue;
		}
		
	</style>
</head>
<body>
	<a href="logout.php">Выйти</a>
	<h1>Список дел для пользователя <?= $_SESSION["name"]?></h1>

	<div>
		<form class="header" action="index.php" method="post">	
			<ul>
				<?php
				foreach ($arrHeader as $key => $header) {?>
				<li class="header">
					<?= $header ?>
					<input type="submit" name=<?php  echo '"or_'. $key .'"'?> value="сортировать"></li>
					<?php	} ?>
					<li class="header">изменить</li>
					<li class="header">удалить</li>
				</ul>
			</form>
			<?php
			foreach ($sqlRes as $item) {	
				?>

				<form class="row" action="index.php" method="post">	
					<ul>
						<?php	foreach ($arrHeader as $key => $header) {	
							if ($key == "destination") {?>
               <li><?php echo $users->getStrSelect($item[$key]);  ?> </li>						
							<?php } else {?>
							<li > <input type=<?php switch ($key){case "is_done":echo "checkbox"; break;
																										case "id";
																										case "date_added";
																										case "autor"   :echo "hidden"; break;
																										default: echo "text";
							} ?> name=<?= $key ?> value=<?php  echo '"'. htmlspecialchars($item[$key]) .'"';
							if (($key=="is_done")&&($item[$key]==1)){echo " checked" ;};?>><?php if (!array_key_exists($key,$editField)){echo $item[$key];} ?>
						</li>							
						<?php } 
						} ?>	
						<li ><input type="submit" name="submitChange" value="Сохранить">	</li>
						<li ><input type="submit" name="submitDel" value="Удалить"></li>
					</ul>	
				</form>
				<?php }?>		
			</div>
			<form action="formEdit.php" method="post">
				<input type="submit" name="submitAdd" value="Создать новое задание">
			</form>
			<h2>Чужие задания</h2>
				<ul>
				<?php
				foreach ($arrHeader as $key => $header) {?>
				  <li class="header"><?= $header ?></li>
				<?php }?>
				</ul>
				<ul>
				<?php
				foreach ($ForeignTasks as $item) {	
						foreach ($arrHeader as $key => $header) {?>
    				  <li><?= $item[$key] ?></li>
				<?php }
				}?>
				
				</ul>
			</form>
		</body>
